# AngularPopUpBox



# Demo 
comming soon



## Feature:
- support multiple style, and customize class.
- support customize function button

## Api
- Angular service format: please check the "api-example.service"
- Angular viewChild please check the "app.component.ts"

## param
- @param message message text
- @param title title of the box
- @param hint a small message shown as a hint * @param style pre-defined style array
- @param duration wait how many seconds to auto close this box. Default 0 to disable auto close.
- @param btnClose name of the close button. Default "Close". Set to empty to hide close button
- @param btnName name of the additional button
- @param btnAction function blinded to the additional button




## Publish

Run `npm run prePublish`,'cd publish','npm run version patch','npm publish'. 


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.4.
