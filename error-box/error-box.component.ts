﻿import { Component, Input, OnInit } from '@angular/core';
import {
    trigger,
    state,
    style,
    transition,
    animate,
    keyframes
} from '@angular/animations';
import { ErrorBoxMsg2 } from './model';

@Component({
    selector: 'error-box',
    styleUrls: ['error-box.component.less'],
    templateUrl: 'error-box.component.html',
    animations: [
        trigger('animationState', [
            transition('void => *', [
                style({
                    opacity: 0,
                    top: '-4rem'
                }),
                animate('0.2s cubic-bezier(0.4, 0.0, 0.2, 1)')
            ]),
            transition('* => void', [
                animate(
                    '0.2s cubic-bezier(0.4, 0.0, 0.6, 1)',
                    style({
                        opacity: 0,
                        top: '-4rem'
                        //    transform: 'scale(0.1)'
                    })
                )
            ])
        ]),
        trigger('animationState2', [
            transition('void => *', [
                style({
                    opacity: 0
                }),
                animate('0.2s cubic-bezier(0.4, 0.0, 0.2, 1)')
            ]),
            transition('* => void', [
                animate(
                    '0.2s cubic-bezier(0.4, 0.0, 0.6, 1)',
                    style({
                        opacity: 0
                    })
                )
            ])
        ])
    ]
})
export class ErrorBoxComponent {
    /**
     * Unused, to support multiple error boxes within one page
     */

    public visible = false;
    public message = '';
    public title = '';
    public hint = '';
    public style: string[] = [];

    // close button name
    public btnClose = 'Close';

    // Additional button name
    public btnName = '';

    // Additional button action
    public btnAction = () => {};

    constructor() {}

    /**
     * Close the message box
     */
    public close(): void {
        this.visible = false;
        setTimeout(() => {
            this.message = this.title = this.hint = '';
            this.btnClose = this.btnName = '';
            this.style = [];
        }, 300);
    }

    /**
     * Show message box with full parameter support
     * @param message message text
     * @param title title of the box
     * @param hint a small message shown as a hint
     * @param style pre-defined style array
     * @param duration wait how many seconds to auto close this box. Default 0 to disable auto close.
     * @param btnClose name of the close button. Default "Close". Set to empty to hide close button
     * @param btnName name of the additional button
     * @param btnAction function blinded to the additional button
     */
    public show(msgObject: ErrorBoxMsg2): void {
        // check parameter
        if (typeof msgObject._btnAction !== 'function') {
            throw new Error('btnAction must be a valid function.');
        }
        this.message = msgObject._message;
        this.title = msgObject._title;
        this.hint = msgObject._hint;
        this.btnClose = msgObject._btnClose;
        this.btnName = msgObject._btnName;
        this.btnAction = msgObject._btnAction;

        if (!msgObject._style || !msgObject._style.length) {
            this.style = [];
            //  this.animationState
        } else {
            this.style = msgObject._style;
        }

        // auto close
        if (msgObject._duration) {
            setTimeout(() => {
                this.close();
            }, msgObject._duration);
        }

        this.visible = true;
    }

    /**
     * Show a modal message box with a Refresh button
     * @param message message text
     */
    public showCritical(message: string): void {
        this.show(
            new ErrorBoxMsg2(
                message,
                'Critical Error',
                null,
                ['overlay', 'alert'],
                0,
                null,
                'Refresh',
                function() {
                    location.reload();
                }
            )
        );
    }

    /**
     * Show a non-modal message box with auto close in milli seconds
     * @param message message text
     * @param duration wait how many seconds to auto close this box. Default 5000. Set to 0 to disable auto close.
     */
    public showNonCritical(message: string, duration = 5000): void {
        this.show(new ErrorBoxMsg2(message, null, null, null, duration));
    }

    /**
     * Show a session timeout message box without any button.
     * @param message original error message
     */
    public showSessionOut(message: string = ''): void {
        console.log(arguments);
        this.show(
            new ErrorBoxMsg2(
                message +
                    '<br>Your session has timed out. Please close your browser and try again from the map URL.',
                'Session Timeout',
                null,
                ['overlay', 'alert'],
                0,
                null,
                null
            )
        );
    }
}
