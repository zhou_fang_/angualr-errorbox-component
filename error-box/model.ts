export class ErrorBoxMsg2 {
    constructor(
        public _message: string = '',
        public _title = '',
        public _hint = '',
        public _style: string[] = [],
        public _duration = 0,
        public _btnClose = 'Close',
        public _btnName = '',
        public _btnAction = () => {}
    ) {}
}
