import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ErrorBoxComponent } from './error-box/error-box.component';
import { ChildComponent } from './child/child.component';
import { ApiExampleService } from './error-box/api-example.service';

@NgModule({
    declarations: [AppComponent, ErrorBoxComponent, ChildComponent],
    imports: [BrowserModule, BrowserAnimationsModule, FormsModule],
    providers: [ApiExampleService],
    bootstrap: [AppComponent]
})
export class AppModule {}
