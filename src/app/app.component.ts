import {
    Component,
    ViewChild,
    OnInit,
    AfterViewInit,
    OnDestroy
} from '@angular/core';
import { ErrorBoxComponent } from './error-box/error-box.component';
import { ApiExampleService } from './error-box/api-example.service';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
    title = 'app';
    errorSubs;
    @ViewChild(ErrorBoxComponent) errorBox: ErrorBoxComponent;
    _message = `The units in your CAD file does not match your coordinate.
    Please redefine the coordinate system or change the units in your CAD file and re-upload it.`;
    _title = 'title' /* 'Message box title' */;
    _hint = '_hint' /*  'click out side to close window' */;
    style = [
        {
            className: 'overlay',
            value: true
        },
        {
            className: 'alert',
            value: false
        },
        {
            className: 'icon',
            value: true
        },
        {
            className: 'solid',
            value: true
        },
        {
            className: 'warning',
            value: false
        },
        {
            className: 'success',
            value: true
        },
        {
            className: 'alert',
            value: false
        },
    ];
    _style: string[] = [];
    _duration = 0;
    _btnClose = 'Ok';
    _btnName = 'retry';
    _btnActionText = '()=>{alert(123)}';
    _btnAction = () => {};
    constructor(private api: ApiExampleService) {}
    apply() {
        console.log(this._btnActionText);
        this._btnAction = eval(this._btnActionText);

        this._style = [];
        this.style.forEach(item => {
            if (item.value === true) {
                this._style.push(item.className);
            }
        });
        this.api.show(
            this._message,
            this._title,
            this._hint,
            this._style,
            this._duration,
            this._btnClose,
            this._btnName,
            this._btnAction
        );
    }

    ngOnInit() {}
    ngAfterViewInit() {
        //    this.api.showSessionOut('abc');
        // this.api.showCritical('bcd');
        this.errorSubs = this.api.MsgEvent.subscribe(reps => {
            //     alert(456);
            console.log(reps);
            setTimeout(() => {
                this.errorBox.show(reps);
            }, 0);
        });
    //    this.apply();
        /*   this.api
            .showCritical(`The units in your CAD file does not match your coordinnate.
        Please redefine the coordinate system or change the uints in your CAD file and re-upload it.`); */
    }
    ngOnDestroy() {
        this.errorSubs.unsubscribe();
    }
}
