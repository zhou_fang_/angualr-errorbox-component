import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { ErrorBoxMsg2 } from './model';
@Injectable()
export class ApiExampleService {
    MsgEvent: Subject<ErrorBoxMsg2> = new Subject();
    /**
     * Show a modal message box with a Refresh button
     * @param message message text
     */
    public show(
        _message: string,
        _title = '',
        _hint = '',
        _style: string[] = [],
        _duration = 0,
        _btnClose = 'Close',
        _btnName = '',
        _btnAction = () => {}
    ): void {
        const ErrorBoxMsgObject = new ErrorBoxMsg2(
            _message,
            _title,
            _hint,
            _style,
            _duration,
            _btnClose,
            _btnName,
            _btnAction
        );
        this.MsgEvent.next(ErrorBoxMsgObject);
    }

    public showCritical(message: string): void {
        this.show(
            message,
            'Critical Error',
            null,
            ['overlay', 'alert'],
            0,
            null,
            'Refresh',
            function() {
                location.reload();
            }
        );
    }

    /**
     * Show a non-modal message box with auto close in million seconds
     * @param message message text
     * @param duration wait how many seconds to auto close this box. Default 5000. Set to 0 to disable auto close.
     */
    public showNonCritical(message: string, duration = 5000): void {
        this.show(message, null, null, null, duration);
    }

    /**
     * Show a session timeout message box without any button.
     * @param message original error message
     */
    public showSessionOut(message: string = ''): void {
        this.show(
            message +
                '<br>Your session has timed out. Please close your browser and try again from the map URL.',
            'Session Timeout',
            null,
            ['overlay', 'alert'],
            0,
            null,
            null
        );
    }
}
